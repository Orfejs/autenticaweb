package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.entity.Entry;

public interface EntryRepo extends JpaRepository<Entry, Long> {
    Entry findByName(String name);
}


