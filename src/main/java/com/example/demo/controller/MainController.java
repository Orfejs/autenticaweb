package com.example.demo.controller;


import com.example.demo.entity.Entry;
import com.example.demo.repository.EntryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class MainController {

    private EntryRepo entryRepo;
    @Autowired
    public MainController(EntryRepo entryRepo) {
        this.entryRepo = entryRepo;
    }

    @GetMapping("/")
    public String home(Model model) {
        return "index";
    }

    @GetMapping("/entries")
    public String showSignUpForm(Entry entry) {
        return "entries";
    }
    @PostMapping("/adduser")
    public String addUser(@Valid Entry user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-user";
        }
        entryRepo.save(user);
        return "redirect:/index";
    }
    }










